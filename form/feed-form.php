<?php
session_start();
include('../function/connect_db.php');

//redirect
if(!isset($_SESSION["auth"])) {
    header('Location: ../index.php');
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Feed Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/feed-form.css" />
    <script src="main.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/bootstrap.min.js"></script>
    <script src="../jquery/jquery-3.3.1.min.js"></script>
</head>
<body class="">
    <nav class="navbar navbar-expand-lg navbar-dark bg-info">
        <div class="container ">
             <a class="navbar-brand pl-4 ml-4" href="#"><h2 class="ml-5 pl-4">Feeds</h2></a>
        <div class="collapse navbar-collapse">                
            <ul class="navbar-nav mr-auto">
            </ul>
            <div class="nav">
                <div class="nav">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="d-flex justify-content-between align-items-center mr-4 pr-4">
                            <div class="pr-5">
                                <img class="" class="rounded-circle" width="55" src="../image/selina.JPG" alt="">  
                            </div>
                            <div class="dropdown mr-2 pr-3">
                                <h2>Selina</h2>  
                            </div>
                                                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="container-fluid gedf-wrapper">
        <div class="row">
            <div class="col-md-2">
                
            </div>
            <div class="col-md-8 gedf-main">

                <!--- \\\\\\\Post-->
                <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                                <div class="form-group form-rounded">
                                    <label class="sr-only" for="message">post</label>
                                    <textarea class="form-control form-rounded" id="message" rows="2" placeholder="Hi Selina, How is your feeling today?"></textarea>
                                </div>

                            </div>
                </div>
                <div class="card gedf-card">
                    
                    
                </div>
                <!-- Post /////-->

                <!--- \\\\\\\Post-->
                <div class="card gedf-card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="55" src="../image/cristina.JPG" alt="">
                                </div>
                                    <div class="ml-2">
                                        <div class="h5 m-0">Christina</div>
                                        
                                    </div>
                                </div>
                            <div class="text-muted h7 mb-2 disable"> <i class="fa fa-clock-o"></i>23 HOURS AGO</div>
                        </div>

                    </div>
                    <div class="card-body">                        
                        <p class="card-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt 
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                             laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
                             voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupicatat
                              non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

                        </p>
                    </div>
                </div>

                <br><!-- line break-->

                <!--- \\\\\\\Post-->
                <div class="card gedf-card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="55" src="../image/justin-small.JPG" alt="">
                                </div>
                                <div class="ml-2">
                                    <div class="h5 m-0">Justin</div>
                                </div>
                            </div>   
                            <div class="text-muted h7 mb-2"> <i class="fa fa-clock-o"></i> 1 DAY AGO</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p class="card-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt 
                            ut labore et dolore magna aliqua.
                        </p>
                        <div><img  src="../image/justin.PNG" alt="justinbieber" width="100%"></div>      
                                         
                    </div>                
                </div>

                <br><!-- line break-->
                <!-- Post /////-->
                <div class="card gedf-card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="55" src="../image/cristina.JPG" alt="">
                                </div>
                                    <div class="ml-2">
                                        <div class="h5 m-0">Christina</div>
                                        
                                    </div>
                                </div>
                            <div class="text-muted h7 mb-2 disable"> <i class="fa fa-clock-o"></i>2 DAYS AGO</div>
                        </div>

                    </div>
                    <div class="card-body">                        
                        <p class="card-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt 
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                             laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
                             voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupicatat 
                             non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

                        </p>
                    </div>
                </div>

                <br><!-- line break-->

                <!--- \\\\\\\Post-->
                <div class="card gedf-card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="55" src="../image/justin-small.JPG" alt="">
                                </div>
                                <div class="ml-2">
                                        <div class="h5 m-0">Justin</div>
                                        
                                    </div>
                            </div>
                            <div>    
                                <div class="text-muted h7 mb-2"> <i class="fa fa-clock-o"></i>1 DAYS AGO</div>                            
                            </div>
                        </div>

                    </div>
                    <div class="card-body">
                        
                        <p class="card-text">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. sed do eiusmod tempor 
                            incididunt ut labore et dolore magna aliqua.
                        </p>
                        <img src="../image/justin.PNG" alt="justinbieber " width="100%">
                    </div>
                </div>



            </div>
            
        </div>
    </div>
</body>
</html>