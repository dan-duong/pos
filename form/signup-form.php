<?php
    include('../function/connect_db.php');

	// if(!isset($_SESSION["auth"])) {
    //     header('Location: ../index.php');
    // }

    if(isset($_POST['submit'])){
        $password = $_POST['password'];
        $confirmPassword =  $_POST['confirmpassword'];
        $email = $_POST['email'];
       
        $sql = "INSERT INTO users1_tbl (email,firstname,lastname,password)
        VALUES ('".$_POST["email"]."','".$_POST["firstname"]."','".$_POST["lastname"]."','".md5($_POST["password"])."'
        )";

        $select = "SELECT * FROM users1_tbl WHERE email = '$email' ";
        $result = mysqli_query($conn, $select);
        if(mysqli_num_rows($result)) {
            ?>
                <script>alert("This email is already used ");</script>
            <?php
            //header('Location: '.$_SERVER['REQUEST_URI']);
        }else{
            if($password == $confirmPassword){
                if (mysqli_query($conn, $sql)){
                    ?>
                        <script>alert("Create success");</script>
                    <?php
                   
                    //header('Location: ../index.php');
                    //header('Location: '.$_SERVER['REQUEST_URI']); //clear 
                } else {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            }else{
                ?>
                    <script>alert("Password doesn't compare");</script>
                <?php
                //header('Location: '.$_SERVER['REQUEST_URI']); //clear 
            }    
        }
                
        }
    

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/signup-style.css" />
    <script src="main.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/bootstrap.min.js"></script>
    <script src="../jquery/jquery-3.3.1.min.js"></script>
    
</head>
<body class="">
   <div >
    <!-- <div class="container login-form">
        <div class="row">
            <div class="col-md-4 login-form-1">
                <h2>SIGN UP</h2>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email*">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="First Name *">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Last Name *">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="password *">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Confirm Password *">
                    </div>
                    <div class="form-group">
                        <input type="checkbox" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="button" class="form-control bg-primary" value="Save" >
                    </div>
                   

            </div>
        </div>
    
    </div> -->
    <!-- <div id="login-box">
        <div class="left-box">
            <h1>SIGN UP</h1>
                <input type="email" name="email" placeholder="Email *">
                <input type="text" name="firstname" placeholder="First Name *">
                <input type="text" name="lastname" placeholder="Last Name *" >
                <input type="password" name="password" placeholder="Password *" >
                <input type="password" name="password" placeholder="Confirm Password *" >
                <input type="submit" name="signup-button" value="SAVE">

        </div>
        <div class="right-box">
            <span class="signinwith"><h1>SIGN IN WITH</h1></span>
            <button class="social facebook">Login With Facebook</button>
            <button class="social twitter">Login With twitter</button>
            <button class="social google">Login With Google</button>
        </div>
        <div class="or">OR</div>
    </div> -->


    <!-- ------------------------ SIGN UP ------------------------------------ -->
        
        <div class="login-form row" >
            <div class="col-xl-1"></div>         
            <form method="post" class="col-xl-10">
                <div class="row ">
                    <div class="col-xl-6 border-right">
                        <h2 class="text-center">SIGN UP</h2>	
                        <div class="form-group frm-signup">
                            <div class="input-group">
                                <input type="email" class="form-control" name="email" placeholder="Email *" required="required">
                            </div>
                        </div>
                        <div class="form-group frm-signup">
                            <div class="input-group">
                                <input type="text" class="form-control" name="firstname" placeholder="First Name *" required="required">
                            </div>
                        </div>
                        <div class="form-group frm-signup">
                            <div class="input-group">
                                <input type="text" class="form-control" name="lastname" placeholder="Last Name *" required="required">
                            </div>
                        </div>
                        <div class="form-group frm-signup">
                            <div class="input-group">
                                <input type="password" class="form-control" name="password" placeholder="Password *" required="required">
                            </div>
                        </div>
                        <div class="form-group frm-signup">
                            <div class="input-group">
                                <input type="password" class="form-control" name="confirmpassword" placeholder="Confirm Password *" required="required">
                            </div>
                        </div>      
                        <div class="clearfix frm-signup ">
                            <label class="pull-left checkbox-inline"><input type="checkbox"> I agree all term and condition</label>
                        </div>   
                        <div class="form-group frm-signup">
                            <button type="submit" class="btn btn-info btn-block save-btn" name="submit">SAVE</button>
                        </div>  
                    </div> 
                    <!-- ------------------------ SIGN UP WITH SOCIAL MEDIA------------------------------------ -->
                    <div class="col-xl-6">
                        <h2 class="text-center">SIGN IN WITH</h2>	
                        <div class="text-center social-btn">
                            <a href="#" class="btn btn-primary btn-block"><i class="fab fa-facebook-f"></i><b> Login with Facebook</b></a>
                            <a href="#" class="btn btn-info btn-block"><i class="fab fa-twitter-square"></i> <b>Login with Twitter</b></a>
                            <a href="#" class="btn btn-danger btn-block"><i class="fab fa-google-plus-g"></i> <b>Login with Google</b></a>
                        </div>
                    </div>
                </div>    
            </form>

        </div>
        <div class="or-seperator"><i>OR</i></div>
</div> 
</body>
</html>
